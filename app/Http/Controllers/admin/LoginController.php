<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use Validator;
use DB;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\Attendance;


class LoginController extends Controller
{


    protected $guard = 'admin';

    public function showLogin(Request $req){
        return view('admin.login');
    }
    public function create_account(Request $request)
    {
        $data  = [
            'failures' =>''
        ];
        return view('admin.account', $data);
    }
    public function insert_account(Request $request)
        {
            $rules = [
                'name' => 'required|regex:/^[a-zA-Z]+$/u|max:255',
                'lname' => 'required|regex:/^[a-zA-Z]+$/u|max:255',
                'email' => 'required|email',
                'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
                'password_confirmation' => 'required| min:8'
            ];
            $messages = [
                'name.required' => 'First Name is required',
                'lname.required' => 'Last Name is required',
                'email.required' => 'Email is required',
                'password.required' => 'Password is required',
                'password_confirmation' => 'Confirm Password is required'
            ];
            $validation = Validator::make($request->all(),$rules,$messages);
            if($validation->fails()){
                return response()->json(['status' => false , 'response' => $validation->errors()]);
            }
            try{

                $admin = new Admin();
                $admin->name = $request->name;
                $admin->email = $request->email;    
                $admin->password = bcrypt($request->password);
                $admin->original_pass = $request->password;
                
                $admin->save();
                return response()->json(['status'=>1,'response'=>"Admin Created Successfully"]);

                } catch (\Exception $e) {
                    throw $e;
                    return response()->json(['status'=>0,'response'=>"Not Inserted Data"]);
                }
        }
     
    public function availability_mail(Request $request)
      {
            $email = $request->email;
            $emailcheck = Admin::where('email',$email)->count();
            if($emailcheck >= 1)
            {
                $arr = array('response' => 'Email  ID is Already Taken!', 'status' => '1');
            }
            else
                {
                    $arr = array('response' => 'Email  ID is Accepted!', 'status' => '0');
                }
                return Response()->json($arr);
        }    

    public function dashboard(){

      
       
        return view('admin.dashboard');
       
    }
    public function login(Request $request){
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
        $messages = [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status' => false , 'response' => $validation->errors()]);
        }
        $remember_me = $request->has('remember_me') ? true : false; 
        if(auth()->guard('admin')
        ->attempt(['email'=>$request->email,'password'=>$request->password], $remember_me)){
            $admin = auth()->guard('admin')->user();
            Session::put('name',$admin->name);
            Session::put('email',$admin->email);
            Session::put('id',$admin->id);
            return response()->json(['status'=>1,'response'=>"Login success"]);
        }else{
            return response()->json(['status'=>0,'response'=>['Invalid Username or Password']]);
        }
    }
    public function get_forgotpassword(Request $request)
        {
            return view('admin.forgot');
        }
    public function forgotpassword(Request $request){

        $data = Admin::where('email',$request->email)->count();
       
        if($data!=0)
        {
        $data_details = Admin::where('email',$request->email)->first();

        $details = [
            'body' => 'Your Password is :'.$data_details->original_pass
        ];
        \Mail::to($request->email)->send(new \App\Mail\MyTestMail($details));
        return response()->json(['status'=>1,'response'=>"Email Send Successfully"]);
    }
    else

        {
            return response()->json(['status'=>0,'response'=>"Invalid Email Address.Please Check it."]);
        }

    }

    public function logout() {
        Auth::logout();
     //   Session::flush();
        return redirect()->route('login');
    }


// Employee

public function employee(Request $request)
    {
        $admin_id = Auth::user()->id;

        $employee  = Employee::where('admin_id',$admin_id)->get();
        $data = [
            'employee' => $employee
        ];
        return view('admin.employee',$data);
    }
public function availability_employee(Request $request)
    {
        $email = $request->email;
        $emailcheck = Employee::where('email',$email)->count();
        if($emailcheck >= 1)
        {
            $arr = array('response' => 'Email  ID is Already Taken!', 'status' => '1');
        }
        else
            {
                $arr = array('response' => 'Email  ID is Accepted!', 'status' => '0');
            }
            return Response()->json($arr);
    }
    
public function insert_employee(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'mobile'   => 'required|numeric|digits:10',
            'designation' => 'required',
            'salary' => 'required',
        ];
        $messages = [
            'name.required' => 'Employee  Name is required',
            'email.required' => 'Email is required',
            'mobile.required' => 'Mobile is required',
            'designation.required' => 'Employee  Designation is required',
            'salary.required' => 'Employee  Salary  is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status' => false , 'response' => $validation->errors()]);
        }
        try{

            $employee = new Employee();
            $employee->name = $request->name;
            $employee->email = $request->email;    
            $employee->mobile =$request->mobile;    
            $employee->designation = $request->designation;
            $employee->salary = $request->salary;
            $employee->admin_id = Auth::user()->id;
            
            $employee->save();
            $arr = array('response' => 'Employee Added Successfully!', 'status' => 1);
            return Response()->json($arr);

            } catch (\Exception $e) {
                throw $e;
                $arr = array('response' => 'Not Insert data!', 'status' => 0);
                return Response()->json($arr);
            }
    } 
    
 public function deleteEmployee(Request $request)
    {
        if($request->id){
            try{
                Employee::where('id',$request->id)->delete();
                $arr = array('response' => 'Employee Deleted  Successfully!', 'status' => 1);
            }catch(\Exception $e){
                $arr = array('response' => 'Not Delete Records!', 'status' => 0);
            }
        }else{
            $arr = array('response' => 'ID Not Passed!', 'status' => 0);
        }
        return Response()->json($arr);
    }
 
public function update_employee(Request $request)
    {
        $update = Employee::where('id',$request->id)->update([
            'name' =>$request->name ,'email' =>$request->email,'mobile' =>$request->mobile,
            'designation' =>$request->designation,'salary' =>$request->salary
        ]);
        if($update == TRUE)
            {
                $arr = array('response' => 'Employee Updated Successfully!', 'status' => 1);
            }
            else
                {
                    $arr = array('response' => 'Employee Not Updated!', 'status' => 0);
                }
                return Response()->json($arr);
    } 
    
    
public function get_employee(Request $request)
    {
        $admin_id = Auth::user()->id;
        $employee  = Employee::where('admin_id',$admin_id)->get();
        $data = [
            'employee' => $employee
        ];
        return view('admin.employee_excel',$data);
    } 
 
// Generate A Temp Attendace  Depend Upon Employee 

public function salary_calculation(Request $request)
    {
        $admin_id = Auth::user()->id;
        $emp_count  = Employee::where('admin_id',$admin_id)->count();
        if($emp_count == 0)
        {
            $arr = array('data' => 'No Records Found!', 'status' => false);
            return Response()->json($arr);
        }
        else
            {
                $employee  = Employee::where('admin_id',$admin_id)->get();
                    foreach($employee as $emp)
                        {
                            for($i=1; $i<=31; $i++)
                                {
                                    $date = $i.'/03/2020';
                                    $count_details = Attendance::where('emp_id',$emp->id)
                                    ->where('date',$date)->count();
                                    if($count_details == 0)
                                    {
                                        $attendance = new Attendance();
                                        $attendance->emp_id = $emp->id;
                                        $attendance->date = $date;    
                                        $attendance->time =mt_rand(0,23).":".str_pad(mt_rand(0,59), 2, "0", STR_PAD_LEFT);;    
                                        $attendance->ontime = rand(0,1);
                                        $attendance->late = rand(0,1);
                                        $attendance->status = 1;
                                        $attendance->month = 3;
                                        $attendance->save();
                                    }
                                   

                                }
                        }
            }
        

    } 
    
    

  public function getsalarydetails(Request $request)
    {
        $admin_id = Auth::user()->id;
        $emp_count  = Employee::where('admin_id',$admin_id)->count();
        if($emp_count == 0)
        {

            return response()->json(['status' => false, 'data' => 'No Records Found!' ]);
        }
        else
            {
                $emp  = Employee::where('admin_id',$admin_id)->get();
                foreach($emp as $emp)
                    {
                        $emp_id = $emp->id;
                        $salary = $emp->salary;
                        $month = 'March';


                        $total_days = Attendance::where('emp_id',$emp->id)
                        ->where('month',3)->count();
                        $absent_days = Attendance::where('emp_id',$emp->id)
                        ->where('month',3)->where('status',2)->count();
                        $present_days = Attendance::where('emp_id',$emp->id)
                        ->where('month',3)->where('status',1)->count();
                        $late_days = Attendance::where('emp_id',$emp->id)
                        ->where('month',3)->where('late',1)->count();
                        $ontime_days = Attendance::where('emp_id',$emp->id)
                        ->where('month',3)->where('ontime',1)->count();
                        //Perday Salary Calculations
                        if($total_days == 0) { $perday_salary =0;}else{
                        $perday_salary_1  = ($salary / $total_days);
                        $perday_salary =  number_format($perday_salary_1, 2, '.', '');
                        }

                        //salary Caculations 

                        $present_days_without_late = Attendance::where('emp_id',$emp->id)
                        ->where('month',3)->where('status',1)->where('ontime',1)->count();
                        if($present_days_without_late>=10)
                            {
                                $total_count_days = floor($present_days_without_late/10);
                                $bonus = $total_count_days * $perday_salary;
                            }
                            else
                                {
                                    $bonus = 0;
                                }
                                
                        $present_days_with_late = Attendance::where('emp_id',$emp->id)
                        ->where('month',3)->where('status',1)->where('late',1)->count();
                        if($present_days_with_late>=2)
                                {
                                    $count_days = floor($present_days_with_late/2);
                                    $deduction = $count_days * $perday_salary;
                                }
                                else
                                    {
                                        $deduction = 0;
                                    }

                       $absent_salary_deduction = $absent_days * $perday_salary; 
                       $present_day_salary = $present_days * $perday_salary;       
                       $overall_amt = $present_day_salary + $bonus;
                        
                       $remaining_salary_1 = $overall_amt - $deduction;
                       $remaining_salary =  number_format($remaining_salary_1, 2, '.', '');


                        $data[]=[
                            'emp_id' =>$emp_id,
                            'month' =>$month,
                            'total_days' =>$total_days,
                            'total_absent_days'=>$absent_days,
                            'total_present_days'=>$present_days,
                            'late' =>$late_days,
                            'ontime' =>$ontime_days,
                            'fixed_salary' =>$salary,
                            'perday_salary' =>$perday_salary,
                            'ontime_bonus' =>$bonus,
                            'latecome_deduction' =>$deduction,
                            'absent_salary_deduction' =>$absent_salary_deduction,
                            'present_day_salary' =>$present_day_salary,
                            'remaining_salary' =>$remaining_salary
                        ];
                       
                    }
                    return json_encode($data);

                    //return response()->json(['status' => true, 'data' => $data ]);
                   
                   
                    //return response()->json(['status' => true, 'data' => $data ]);
            }
          

    }  





}
