@extends('admin.layouts.app')
@section('content')

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-pagetitle">
  <!-- <i class="icon ion-ios-home-outline"></i> -->
  <div>
    <h4>Dashboard</h4>

  </div>
</div>







<footer class="br-footer" style="visibility:hidden;">
  <div class="footer-left">
    <div class="mg-b-2">Copyright &copy; 2017. Bracket Plus. All Rights Reserved.</div>
    <div>Attentively and carefully made by ThemePixels.</div>
  </div>
  <div class="footer-right d-flex align-items-center">
    <span class="tx-uppercase mg-r-10">Share:</span>
    <a target="_blank" class="pd-x-5"
      href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i
        class="fa fa-facebook tx-20"></i></a>
    <a target="_blank" class="pd-x-5"
      href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i
        class="fa fa-twitter tx-20"></i></a>
  </div>
</footer>
<!-- ########## END: MAIN PANEL ########## -->
@endsection
@section('scripts')
<script>
  $('#dashboard').addClass('active')
  $('#site_title').html(' | Dashboard ')
</script>
<script src="{{asset('assets/lib/d3/d3.js')}}"></script>
<script src="{{asset('assets/lib/rickshaw/rickshaw.min.js')}}"></script>
<script src="{{asset('assets/lib/Flot/jquery.flot.js')}}"></script>
<script src="{{asset('assets/lib/Flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('assets/lib/flot-spline/jquery.flot.spline.js')}}"></script>
<script src="{{asset('assets/lib/jquery.sparkline.bower/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/lib/echarts/echarts.min.js')}}"></script>
<script src="{{asset('assets/lib/select2/js/select2.full.min.js')}}"></script>

<script>
  $(function () {
    'use strict'

    // FOR DEMO ONLY
    // menu collapsed by default during first page load or refresh with screen
    // having a size between 992px and 1299px. This is intended on this page only
    // for better viewing of widgets demo.
    $(window).resize(function () {
      minimizeMenu();
    });

    minimizeMenu();

    function minimizeMenu() {
      if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
        // show only the icons and hide left menu label by default
        $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
        $('body').addClass('collapsed-menu');
        $('.show-sub + .br-menu-sub').slideUp();
      } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
        $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
        $('body').removeClass('collapsed-menu');
        $('.show-sub + .br-menu-sub').slideDown();
      }
    }
  });
</script>

@endsection('scripts')