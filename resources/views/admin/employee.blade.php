@extends('admin.layouts.app')
<style>
.br-section-wrapper-form {
    background-color: #e9ecef;
    padding: 30px 20px;
  
    border-radius: 3px;
}
</style>
@section('content')

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"; rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js";></script> -->

      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
          <span class="breadcrumb-item active">Employee Management</span>
        </nav>
      </div><!-- br-pageheader -->
     
                            <script>
                           function isNumberKey(evt){ 
                                var charCode = (evt.which) ? evt.which : event.keyCode 
                                if (charCode > 31 && (charCode < 48 || charCode > 57)) 
                                    return false; 
                                return true; 
                            } 
                            </script>  

<br>





      <div class="br-pagebody">
        <div class="br-section-wrapper-form">
          

          <div class="table-wrapperbd">
          <form action="javascript:;" id="employee-form" method="POST">
                   <div class="row">

                        <div class="col-lg-4">
                            <div class="custom-file">
                              <label>Name</label>
                                <input type="text" name="name"  class="form-control" placeholder="Employee Name" autocomplete="off" >
                                </div>
                             
                        </div>


                        <div class="col-lg-4">
                            <div class="custom-file">
                            <label>Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Employee Email" autocomplete="off"   id="cemail"  onblur="checkmain(this.value)" >
                            </div>
                            <div class="msgDiv"></div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-file">
                            <label>Mobile</label>
                                <input type="text" name="mobile" class="form-control" placeholder="Employee Mobile" autocomplete="off"   >
                            </div>
                        </div>
                      
                      
                      
                      
                    </div>    
<br><br><br>
                    <div class="row">

                    <div class="col-lg-3">
                            <div class="custom-file">
                            <label>Designation</label>
                            <input type="text" name="designation" id="designation" class="form-control" placeholder="Employee Designation" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="custom-file">
                            <label>Salary</label>
                            <input type="text" name="salary" id="salary" class="form-control" placeholder="Employee Salary" onkeypress="return isNumberKey(event)" autocomplete="off"  >
                            </div>
                        </div>

                   
                        
                      
                        <div class="col-lg-3" style="padding:28px;">
                     
                        <div class="msgDiv1"></div>
                               
                      
                    </div>     </div>


            </form>  

          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->



@if ($errors->any())
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif
<br>




      

  



      <div class="br-pagebody">
        <div class="br-section-wrapper bd">
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
            <thead>
                <tr>
                <th class="">ID</th>
                  <th class="">Name</th>
                  <th class="">Email</th>
                  <th class="">Mobile</th>
                  <th class="">Designation</th>
                  <th class="">Salary</th>
                  <th class="">Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($employee as $row)
                <tr>  
                <td>{{$loop->iteration}}</td>
                  <td>{{$row->name}}</td>
                  <td>{{$row->email}}</td>
                  <td>{{$row->mobile}}</td>
                  <td>{{$row->designation}}</td>
                  <td>{{$row->salary}}</td>
                  <td>
                       <button class="btn btn-outline-primary btn-xs edit-button" onclick="editCustomerBalance('{{$row->id}}', '{{$row->name}}', '{{$row->email}}', '{{$row->mobile}}', '{{$row->designation}}', '{{$row->salary}}')" title="Edit">
                       <i class="fa fa-pencil" title="Edit" data-toggle="tooltip"></i>
                       </button>

                       <button class="btn btn-outline-danger btn-xs delete-button" data-url="{{route('deleteEmployee',['id'=>$row->id])}}">
                           <i class="fa fa-trash-o" title="Delete" data-toggle="tooltip"></i>
                       </button>
                    </td>
                 
                </tr>

            @endforeach
            </table>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->







      <footer class="br-footer">
        <div class="footer-left">
         
        </div>
        <div class="footer-right d-flex align-items-center">
           {{--<span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-twitter tx-20"></i></a> --}}
        </div>
      </footer>

        <!-- Edit Modal -->
      <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title text-center" id="myModalLabel">Edit  Details</h4>
            </div>
            <form class="form-client" id="editForm" action="javascript:;">
              <div class="modal-body">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="id" id="id" value="" style="display:none;" >
                    <input class="form-control" id="edit_name" name="name" type="text" autocomplete="off" >
                  </div>

                  <div class="form-group">
                    <label>Email</label>
                  
                    <input class="form-control" id="edit_email" name="email" type="text" autocomplete="off" >
                  </div>
                  <div class="form-group">
                    <label>Mobile </label>
                    <input class="form-control" id="edit_mobile" name="mobile" type="text"  autocomplete="off" >
                  </div>
                  <div class="form-group">
                    <label>Designation</label>
                    <input class="form-control" id="edit_designation" name="designation" type="text" onkeypress="return isNumberKey(event)" autocomplete="off" >
                  </div>
                  <div class="form-group">
                    <label>Salary</label>
                  <input class="form-control" id="edit_salary" name="salary" type="text" autocomplete="off" >
                 
                  </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.reload();">Close</button>
                <button type="submit" class="btn btn-primary button-save updateBtn">Update</button>
              </div>
           </form>
          </div>
        </div>
      </div>

        <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content delete-popup">
          <div class="modal-header">
            <h4 class="modal-title">Delete Employee Record</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to delete?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="window.location.reload();">Close</button>
            <button type="button" class="btn btn-primary delete-class deleteBtn" data-delete-url="">Delete</button>
          </div>
        </div>
      </div>
    </div>
    
@endsection
@section('scripts')
    <script>
         $('#management').addClass('active')
        $('#employee').addClass('active')
        $('#site_title').html(' | Employee Management ')
    </script>
    <script>
        $('#file').on('change',function(){
          //get the file name
          var fileName = $(this).val();
          //replace the "Choose a file" label
          $(this).next('#file_label').html(fileName);
        });
    </script>
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
        $('#employee-form').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
            
            submitHandler : function(form){
                var form = document.getElementById('employee-form');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('insert_employee')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Saving..");  
                    },
                    success : function(data){
                        $('.addBtn').text('Add Emplyee')
                        $('.addBtn').prop('disabled', false)
                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                            location.href=""
                            }, 1000);
                        }else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })

        $(function(){
          'use strict';

          $('#datatable1').DataTable({
            stateSave: true,
            responsive: true,
            language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
              
            },
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bLengthChange" : false, 
          });

          // Select2
          $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

        function editCustomerBalance(id, name, email , mobile , designation ,salary) {
              $('#editForm').trigger('reset')
              $("input[name='name']").val(name)
             
              $("input[name='id']").val(id)
              $("input[name='email']").val(email)
              $("input[name='mobile']").val(mobile)
              $("input[name='designation']").val(designation)
              $("input[name='salary']").val(salary)
        
              $("#editModal").modal({
                backdrop: 'static',
                keyboard: false
              });
        }

        $('#editForm').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
          
            submitHandler : function(form){
                var form = document.getElementById('editForm');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('update_employee')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false,
                    cache: false, 
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Updating..");  
                    },
                    success : function(data){
                        $('.addBtn').text('Update')
                        $('.addBtn').prop('disabled', false)
                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                                window.location.href=""
                            }, 1000);
                        }else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })
      
        
        $(".delete-button").click(function(){
            $("#deleteModal").modal();
            $('.delete-class').data('delete-url', $(this).data('url'));
        });
        $(".delete-class").click(function(){
            var url = $(this).data('delete-url');
            $('.deleteBtn').prop('disabled', true)
            $.ajax({
                type : "GET",
                url : url,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                  $(".deleteBtn").text("Deleting..");  
                },
                success: function(data) {
                    $(".deleteBtn").text("Delete"); 
                    $('.deleteBtn').prop('disabled', false)
                    if (data.status == true) {
                        toastr["success"](data.response);
                        setTimeout(function() {      
                                window.location.href=""
                        },1000);
                    } else {
                        if (data.response) {
                        toastr["error"](data.response);
                        }
                    }
                }
            })
        })

        $(function(){
          'use strict'

          // FOR DEMO ONLY
          // menu collapsed by default during first page load or refresh with screen
          // having a size between 992px and 1299px. This is intended on this page only
          // for better viewing of widgets demo.
          $(window).resize(function(){
            minimizeMenu();
          });

          minimizeMenu();

          function minimizeMenu() {
            if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
              // show only the icons and hide left menu label by default
              $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
              $('body').addClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideUp();
            } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
              $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
              $('body').removeClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideDown();
            }
          }
        });
    </script>
    <script>$(document).ready(function() {
    $('#js-example-basic-single').select2();
});</script>


<script>
    function checkmain(email)
{
  
$.ajax({
  url:"{{ route('availability_employee') }}",
type: 'POST',
data: { email: email ,  "_token": "{{ csrf_token() }}" },
}).done(function(data) {
  if(data.status == 1)
  {
  $('.msgDiv').html(`<div style="color:red;">` + data.response + `</div>`);
  $('.msgDiv1').html(`<button disabled type="submit"  id="" class="btn btn-primary addBtn">Add Employee</button>`);

  
  }
  else
    {
      $('.msgDiv').html(`<div  style="color:green;">` + data.response + `</div>`);
      $('.msgDiv1').html(`<button type="submit"  id="" class="btn btn-primary addBtn">Add Employee</button>`);
    }


});
}
</script>


@endsection