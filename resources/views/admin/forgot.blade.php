<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Admin-Forgot Password</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <!-- Twitter -->
        <meta name="twitter:site" content="@themepixels">
        <meta name="twitter:creator" content="@themepixels">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Bracket Plus">
        <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <!-- Facebook -->
        <meta property="og:url" content="http://themepixels.me/bracketplus">
        <meta property="og:title" content="Bracket Plus">
        <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="600">
        <!-- Meta -->
        <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="author" content="ThemePixels"> --}}
  
        <!-- vendor css -->
        <link href="{{asset('assets/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
        <!-- Bracket CSS -->
        <!-- <link rel="stylesheet" href="{{asset('assets/css/gold.css')}}"> -->
        <link rel="stylesheet" href="{{asset('assets/css/bracket.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bracket.oreo.css')}}">
    </head>
    <body>
        <div class="d-flex align-items-center justify-content-center ht-100v">
      
            <form role="form" method="post" action="javascript:;" id="forgotForm">
                <div class="overlay-body d-flex align-items-center justify-content-center">
                    <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bd bd-white-2">
                    <div class="tx-white-5 tx-center mg-b-25">Enter Your Mail</div>
                    <h6>
                    <div class="msgDiv"></div>
                    </h6>
                    <div class="form-group">
                    <label for="">Email</label>
                        <input type="email" name="email" id="email" class="form-control fc-outline-dark" placeholder="Enter your email">
                    </div><!-- form-group -->
                  
                    <div class="row">
                       <div class="col-md-6">
                       <a href="{{route('create_account')}}" class="tx-primary tx-12 d-block mg-t-10" >Create Account</a>
                       </div>
                       <div class="col-md-6">
                       <a href="{{route('get_forgotpassword')}}" class="tx-primary tx-12 d-block mg-t-10" >Forgot password?</a>
                       </div>
                       </div>

<br>


                    <button type="submit" class="btn btn-white btn-block">Send Password</button>

                
                    </div><!-- login-wrapper -->
                </div><!-- overlay-body -->
            </form>
        </div><!-- d-flex -->


        <script src="{{asset('assets/lib/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
        <script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- <script src="{{asset('js/jquery.validate.js')}}"></script> -->
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script>
            $("#forgotForm").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                  
                },
                messages: {
                    email: {
                        required: 'Email is  required',
                        email: 'Email is invalid'
                    },
                  
                },
                submitHandler: function (form) {
                    var form = document.getElementById('forgotForm');
                    var data = new FormData(form);
                    $('.msgDiv').html(``);
                    $.ajax({
                        type: "POST",
                        url: "{{route('forgotpassword')}}",
                        data: $(form).serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data){


                            if (data.status == 1) {

                                $('.msgDiv').html(` <div class="alert alert-danger">` + data.response + `</div> `);
                                setTimeout(function () {
                                    location.href = "{{route('login')}}";
                                }, 3000);


                            } else {
                               
                                $('.msgDiv').html(` <div class="alert alert-danger">` + data.response + `</div> `);
                                setTimeout(function () {
                                  window.location.reload();
                                }, 3000);
                            }
                        }
                    });
                    return false;
                }
            });
        </script>    
    </body>
</html>