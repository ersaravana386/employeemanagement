@extends('admin.layouts.app')
<style>
.br-section-wrapper-form {
    background-color: #e9ecef;
    padding: 30px 20px;
  
    border-radius: 3px;
}
</style>
@section('content')
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
          <span class="breadcrumb-item active">Employee Management</span>
        </nav>
      </div><!-- br-pageheader -->





      <div class="br-pagebody">
        <div class="br-section-wrapper bd">
          <div class="table-wrapper">
          <table id="examplemm" class="table display responsive nowrap">
              <thead>
                <tr>
                <th class="">ID</th>
                  <th class="">Name</th>
                  <th class="">Email</th>
                  <th class="">Mobile</th>
                  <th class="">Designation</th>
                  <th class="">Salary</th>
                 
                </tr>
              </thead>
              <tbody>
              @foreach($employee as $row)
                <tr>
                 <td>{{$loop->iteration}}</td>
                  <td>{{$row->name}}</td>
                  <td>{{$row->email}}</td>
                  <td>{{$row->mobile}}</td>
                  <td>{{$row->designation}}</td>
                  <td>{{$row->salary}}</td>
                </tr>


                




             @endforeach   

           
            </table>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->


      <footer class="br-footer">
        <div class="footer-left">
         
        </div>
        <div class="footer-right d-flex align-items-center">
           {{--<span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-twitter tx-20"></i></a> --}}
        </div>
      </footer>

     
@endsection
@section('scripts')
    <script>
        $('#management').addClass('active')
        $('#get_employee').addClass('active')
        $('#site_title').html(' | Employee Management ')
    </script>
    <script>
        $('#file').on('change',function(){
          //get the file name
          var fileName = $(this).val();
          //replace the "Choose a file" label
          $(this).next('#file_label').html(fileName);
        });
    </script>
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
        $('#employee-form').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
            
            submitHandler : function(form){
                var form = document.getElementById('employee-form');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('insert_employee')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Saving..");  
                    },
                    success : function(data){
                        $('.addBtn').text('Add Emplyee')
                        $('.addBtn').prop('disabled', false)
                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                            location.href=""
                            }, 1000);
                        }else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })

        $(function(){
          'use strict';

          $('#datatable1').DataTable({
            responsive: true,
            language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
            },
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bLengthChange" : false, 
          });

          // Select2
          $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

       

        $('#editForm').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
           
            submitHandler : function(form){
                var form = document.getElementById('editForm');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('update_employee')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false,
                    cache: false, 
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Updating..");  
                    },
                    success : function(data){

                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                                window.location.href=""
                            }, 1000);
                        }else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })
      
        
        $(".delete-button").click(function(){
            $("#deleteModal").modal();
            $('.delete-class').data('delete-url', $(this).data('url'));
        });
        $(".delete-class").click(function(){
            var url = $(this).data('delete-url');
            $('.deleteBtn').prop('disabled', true)
            $.ajax({
                type : "GET",
                url : url,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                  $(".deleteBtn").text("Changing..");  
                },
                success: function(data) {
                    $(".deleteBtn").text("Changed"); 
                    $('.deleteBtn').prop('disabled', false)
                    if (data.status == true) {
                        toastr["success"](data.response);
                        setTimeout(function() {      
                                window.location.href=""
                        },1000);
                    } else {
                        if (data.response) {
                        toastr["error"](data.response);
                        }
                    }
                }
            })
        })

        $(function(){
          'use strict'

          // FOR DEMO ONLY
          // menu collapsed by default during first page load or refresh with screen
          // having a size between 992px and 1299px. This is intended on this page only
          // for better viewing of widgets demo.
          $(window).resize(function(){
            minimizeMenu();
          });

          minimizeMenu();

          function minimizeMenu() {
            if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
              // show only the icons and hide left menu label by default
              $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
              $('body').addClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideUp();
            } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
              $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
              $('body').removeClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideDown();
            }
          }
        });
    </script>

    <script>
    function checkmain(email)
{
  
$.ajax({
  url:"{{ route('availability_employee') }}",
type: 'POST',
data: { email: email ,  "_token": "{{ csrf_token() }}" },
}).done(function(data) {
  if(data.status == 1)
  {
  $('.msgDiv').html(`<div style="color:red;">` + data.response + `</div>`);
  $('.msgDiv1').html(`<button disabled type="submit"  id="" class="btn btn-primary addBtn">Add Employee</button>`);

  
  }
  else
    {
      $('.msgDiv').html(`<div  style="color:green;">` + data.response + `</div>`);
      $('.msgDiv1').html(`<button type="submit"  id="" class="btn btn-primary addBtn">Add Employee</button>`);
    }


});
}
</script>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <script>
	$(document).ready(function() {
	    var table = $('#examplemm').DataTable( {
	        lengthChange: false,
            "language": {
            "paginate": {
            "previous": "p",
            "next": "N"
            }
        },
          buttons: [
          { extend: 'excel',text: '<b  style="color:white;">Excel</b>', filename: 'Employee Report'},
          { extend: 'csv',text: '<b  style="color:white;">CSV</b>', filename: 'Employee Report'},
          { extend: 'pdf',text: '<b  style="color:white;">PDF</b>', filename: 'Employee Report',pageSize: 'LEGAL',},
         ]
	       // buttons: [ 'copy', 'excel', 'csv', 'pdf', 'colvis' ]
	    } );
	 
	    table.buttons().container()
	        .appendTo( '#examplemm_wrapper .col-md-6:eq(0)' );
	} );
     </script>


@endsection