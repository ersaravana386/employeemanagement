<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\admin\LoginController;


Route::get('/',[LoginController::class,'showLogin']);
Route::get('/login',[LoginController::class,'showLogin'])->name('showlogin');
Route::post('/login',[LoginController::class,'login'])->name('login');
Route::get('/get_forgotpassword',[LoginController::class,'get_forgotpassword'])->name('get_forgotpassword');
Route::post('/forgotpassword',[LoginController::class,'forgotpassword'])->name('forgotpassword');
Route::get('/create_account',[LoginController::class,'create_account'])->name('create_account');
Route::post('/insert_account',[LoginController::class,'insert_account'])->name('insert_account');
Route::post('/availability_mail',[LoginController::class,'availability_mail'])->name('availability_mail');


Route::group(['middleware' => 'auth:admin'], function(){
    Route::get('/dashboard',[LoginController::class,'dashboard'])->name('dashboard');
    Route::get('/employee',[LoginController::class,'employee'])->name('employee');
    Route::post('/availability_employee',[LoginController::class,'availability_employee'])->name('availability_employee');
    Route::post('/insert_employee',[LoginController::class,'insert_employee'])->name('insert_employee');
    Route::get('/deleteEmployee',[LoginController::class,'deleteEmployee'])->name('deleteEmployee');
    Route::post('/update_employee',[LoginController::class,'update_employee'])->name('update_employee');
    Route::get('/get_employee',[LoginController::class,'get_employee'])->name('get_employee');

    // Json Response 
    Route::get('/salary_calculation',[LoginController::class,'salary_calculation'])->name('salary_calculation');
    Route::get('/getsalarydetails',[LoginController::class,'getsalarydetails'])->name('getsalarydetails');

    

    Route::get('/logout',[LoginController::class,'logout'])->name('logout');

});


