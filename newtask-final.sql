-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2021 at 03:38 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newtask`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_pass` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `original_pass`, `remember_token`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Murugan', 'ersaravana386@gmail.com', '$2y$10$eKW0tyE90IliPuL9OIjT8.iH.jwkMF9GfVVeA8HCKByV.PU9qkLXK', 'Admin@1234', 'G9hreUSLBVfRdBzOzs2XaIj1FjDQ8NxaRmjscPjq4VYuiatopOant50WMtqt', 1, '2021-03-13 23:29:16', '2021-03-13 23:29:16'),
(2, 'a', 'ss@gmail.com', '$2y$10$ji6gvzdaedLIY4xZpzMV1Ovb022lHqQ/0GSam/Xtplltf5Nnqb0DW', 'Admin@456', NULL, 1, '2021-03-13 23:30:10', '2021-03-13 23:30:10'),
(3, 'asas', 'l@gmail.com', '$2y$10$WkF9mhj/mYXkp6CgB8/VrOxUjXlkUECAchD9hUVmtWWsesY1kdD56', 'Admin@321Q!', NULL, 1, '2021-03-14 10:43:40', '2021-03-14 10:43:40'),
(4, 'Murugan', 'admin386@gmail.com', '$2y$10$KKWaZ4ribtth3wCFsmihEuEkJx1ORRacPrUDO5Dlx0sHWKq4VQ9ta', 'Admin@852Q!', NULL, 1, '2021-03-14 10:48:38', '2021-03-14 10:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ontime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `late` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `month` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `emp_id`, `date`, `remark`, `time`, `ontime`, `late`, `status`, `month`, `created_at`, `updated_at`) VALUES
(1, 1, '1/03/2020', NULL, '6:42', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(2, 1, '2/03/2020', NULL, '9:46', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(3, 1, '3/03/2020', NULL, '3:51', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(4, 1, '4/03/2020', NULL, '20:52', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(5, 1, '5/03/2020', NULL, '20:11', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(6, 1, '6/03/2020', NULL, '12:37', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(7, 1, '7/03/2020', NULL, '13:12', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(8, 1, '8/03/2020', NULL, '2:48', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(9, 1, '9/03/2020', NULL, '11:53', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(10, 1, '10/03/2020', NULL, '4:57', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(11, 1, '11/03/2020', NULL, '3:48', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(12, 1, '12/03/2020', NULL, '5:20', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(13, 1, '13/03/2020', NULL, '12:41', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(14, 1, '14/03/2020', NULL, '6:28', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(15, 1, '15/03/2020', NULL, '22:45', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(16, 1, '16/03/2020', NULL, '21:24', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(17, 1, '17/03/2020', NULL, '14:14', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(18, 1, '18/03/2020', NULL, '12:46', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(19, 1, '19/03/2020', NULL, '0:28', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(20, 1, '20/03/2020', NULL, '15:40', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(21, 1, '21/03/2020', NULL, '14:07', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(22, 1, '22/03/2020', NULL, '3:16', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(23, 1, '23/03/2020', NULL, '1:44', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(24, 1, '24/03/2020', NULL, '7:20', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(25, 1, '25/03/2020', NULL, '23:36', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(26, 1, '26/03/2020', NULL, '21:08', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(27, 1, '27/03/2020', NULL, '15:44', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(28, 1, '28/03/2020', NULL, '7:06', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(29, 1, '29/03/2020', NULL, '10:07', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(30, 1, '30/03/2020', NULL, '5:43', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(31, 1, '31/03/2020', NULL, '11:23', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(32, 7, '1/03/2020', NULL, '1:44', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(33, 7, '2/03/2020', NULL, '7:38', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(34, 7, '3/03/2020', NULL, '1:54', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(35, 7, '4/03/2020', NULL, '12:10', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(36, 7, '5/03/2020', NULL, '18:24', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(37, 7, '6/03/2020', NULL, '21:39', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(38, 7, '7/03/2020', NULL, '0:51', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(39, 7, '8/03/2020', NULL, '5:47', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(40, 7, '9/03/2020', NULL, '18:09', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(41, 7, '10/03/2020', NULL, '2:46', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(42, 7, '11/03/2020', NULL, '0:49', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(43, 7, '12/03/2020', NULL, '3:12', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(44, 7, '13/03/2020', NULL, '16:13', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(45, 7, '14/03/2020', NULL, '22:54', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(46, 7, '15/03/2020', NULL, '9:49', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(47, 7, '16/03/2020', NULL, '7:25', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(48, 7, '17/03/2020', NULL, '21:24', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(49, 7, '18/03/2020', NULL, '2:43', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(50, 7, '19/03/2020', NULL, '4:13', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(51, 7, '20/03/2020', NULL, '17:21', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(52, 7, '21/03/2020', NULL, '12:49', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(53, 7, '22/03/2020', NULL, '5:25', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(54, 7, '23/03/2020', NULL, '19:58', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(55, 7, '24/03/2020', NULL, '13:05', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(56, 7, '25/03/2020', NULL, '21:44', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(57, 7, '26/03/2020', NULL, '1:47', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(58, 7, '27/03/2020', NULL, '18:45', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(59, 7, '28/03/2020', NULL, '14:40', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(60, 7, '29/03/2020', NULL, '3:31', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(61, 7, '30/03/2020', NULL, '12:59', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(62, 7, '31/03/2020', NULL, '14:01', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(63, 8, '1/03/2020', NULL, '7:36', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(64, 8, '2/03/2020', NULL, '18:43', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(65, 8, '3/03/2020', NULL, '20:23', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(66, 8, '4/03/2020', NULL, '5:15', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(67, 8, '5/03/2020', NULL, '5:06', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(68, 8, '6/03/2020', NULL, '21:39', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(69, 8, '7/03/2020', NULL, '3:33', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(70, 8, '8/03/2020', NULL, '11:23', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(71, 8, '9/03/2020', NULL, '18:55', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(72, 8, '10/03/2020', NULL, '19:43', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(73, 8, '11/03/2020', NULL, '5:05', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(74, 8, '12/03/2020', NULL, '20:34', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(75, 8, '13/03/2020', NULL, '8:17', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(76, 8, '14/03/2020', NULL, '6:52', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(77, 8, '15/03/2020', NULL, '1:06', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(78, 8, '16/03/2020', NULL, '8:26', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(79, 8, '17/03/2020', NULL, '1:59', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(80, 8, '18/03/2020', NULL, '17:28', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(81, 8, '19/03/2020', NULL, '12:18', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(82, 8, '20/03/2020', NULL, '13:46', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(83, 8, '21/03/2020', NULL, '3:46', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(84, 8, '22/03/2020', NULL, '12:04', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(85, 8, '23/03/2020', NULL, '20:57', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(86, 8, '24/03/2020', NULL, '4:45', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(87, 8, '25/03/2020', NULL, '18:39', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(88, 8, '26/03/2020', NULL, '17:41', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(89, 8, '27/03/2020', NULL, '20:44', '0', '0', 2, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(90, 8, '28/03/2020', NULL, '2:33', '0', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(91, 8, '29/03/2020', NULL, '17:11', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(92, 8, '30/03/2020', NULL, '2:33', '1', '1', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09'),
(93, 8, '31/03/2020', NULL, '15:46', '1', '0', 1, '3', '2021-03-14 06:35:09', '2021-03-14 06:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `mobile`, `designation`, `salary`, `admin_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Murugan2', 'er@gmail.com', '8056341625', 'PHP', '96000', 1, 1, '2021-03-14 00:10:41', '2021-03-14 21:07:31'),
(7, 'name21', 'name2@gmail.com', '9871234568', 'JAVA', '987000', 1, 1, '2021-03-14 01:03:31', '2021-03-14 02:00:00'),
(8, 'Name32', 'uu@gmail.com', '8056341625', 'PHP', '55000', 1, 1, '2021-03-14 01:43:25', '2021-03-14 21:07:20'),
(13, 'kk', 'lk@gmail.com', '8610363201', 'PHP', '987500', 4, 1, '2021-03-14 20:59:24', '2021-03-14 20:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2021_03_13_190553_create_admins', 1),
(7, '2021_03_14_050545_create_employees', 2),
(8, '2021_03_14_111102_create_attendance', 3),
(10, '2016_06_01_000001_create_oauth_auth_codes_table', 4),
(11, '2016_06_01_000002_create_oauth_access_tokens_table', 4),
(12, '2016_06_01_000003_create_oauth_refresh_tokens_table', 4),
(13, '2016_06_01_000004_create_oauth_clients_table', 4),
(14, '2016_06_01_000005_create_oauth_personal_access_clients_table', 4),
(15, '2021_03_14_114451_create_attendances', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
